<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Technical Documentation | PKYLE</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Technical Documentation Webpages" />
    <meta
      name="keywords"
      content="Kyle Phillips, KyStyle, Documentation, FreeCodeCamp"
    />
    <meta name="author" content="Kyle Phillips" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Technical Documentation" />
    <meta property="og:description" content="Technical Documentation Page" />
    <meta property="og:url" content="https://pkyle.me/fcc/documentation.html" />
    <meta property="og:image" content="/thumb.png" />
    <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
    <link rel="alternate icon" href="/favicon.ico" />
    <link rel="mask-icon" href="/favicon.svg" color="#222831" />
    <link rel="stylesheet" href="documentation.css" />
    <link
      rel="preload"
      as="font"
      href="/fonts/roboto/Roboto-Bold.woff2"
      type="font/woff2"
      crossorigin="anonymous"
    />
    <link
      rel="preload"
      as="font"
      href="/fonts/roboto/Roboto-Regular.woff2"
      type="font/woff2"
      crossorigin="anonymous"
    />
  </head>

  <body>
    <nav id="navbar">
      <header>Minecraft Server Guide</header>
      <a href="#getting_started" class="nav-link">Getting Started</a>
      <a href="#server_configuration" class="nav-link">Server Configuration</a>
      <a href="#gathering_device_information" class="nav-link"
        >Gathering Device Information</a
      >
      <a href="#router_configuration" class="nav-link">Router Configuration</a>
      <a href="#configuring_the_server_properties" class="nav-link"
        >Configuring the Server Properties</a
      >
      <a href="#launching_the_server" class="nav-link">Launching the Server</a>
      <!-- <li><a href="#connecting_to_the_server" class="nav-link">Connecting to the Server</a></li> -->
    </nav>
    <main id="main-doc">
      <section id="hero">
        <div>
          <h1>How to set up a Minecraft Server</h1>
          <p>A guide by Kyle Phillips</p>
        </div>
      </section>
      <section class="main-section" id="getting_started">
        <header>
          <h2>
            Getting Started<a class="header-link" href="#getting_started">#</a>
          </h2>
        </header>
        <h3>Prerequisites</h3>
        <p>
          The Minecraft Server executable requires the Java Runtime Environment
          to be installed on your computer. If you do not have Java installed,
          or think you may have an outdated version, you can download the latest
          version <a href="https://java.com/en/download/">here</a>.
        </p>
        <h3>Setting up the server files</h3>
        <p>Firstly, you will need the server files.</p>
        <ol>
          <li>
            <p>
              Go to the
              <a href="https://www.minecraft.net/en-us/download/server"
                >Minecraft Server Download page</a
              >
              and download the
              <code class="inline-code"
                >minecraft_server.&lt;version&gt;.jar</code
              >
              file
            </p>
          </li>
          <li>
            <p>
              Create a new folder with a memorable name (e.g.
              <i>Minecraft Server 1.17</i>) and move the
              <code class="inline-code"
                >minecraft_server.&lt;version&gt;.jar</code
              >
              file into your new folder.
            </p>
          </li>
          <li>
            <p>
              Open the
              <code class="inline-code"
                >minecraft_server.&lt;version&gt;.jar</code
              >
              file with Java by double clicking on it.
            </p>
          </li>
        </ol>
        <p>
          You should now see some new files appear in your server directory.
        </p>
        <img
          src="/fcc/img/server-files.png"
          alt='In the server directory there is a subdirectory titled "logs", and "eula.txt", "server.jar", and "server.properties" files'
        />
      </section>
      <section class="main-section" id="server_configuration">
        <header>
          <h2>
            Server Configuration<a
              class="header-link"
              href="#server_configuration"
              >#</a
            >
          </h2>
        </header>
        <p>
          To host a Minecraft Server, you must agree to the End User License
          Agreement (EULA)
        </p>
        <ol>
          <li>
            <p>
              Inside of your server directory, you should be able to see a
              <code class="inline-code">eula.txt</code> file. Double click on
              this file to open it.
            </p>
          </li>
          <li>
            <p>
              Read the contents of the file. Most Importantly, read the
              <a href="https://account.mojang.com/documents/minecraft_eula"
                >Minecraft EULA</a
              >.
            </p>
          </li>
          <li>
            <p>
              Only if you agree to the EULA, change
              <code class="inline-code">eula=false</code> to
              <code class="inline-code">eula=true</code>.
            </p>
            <p>
              (If you don’t want to agree to the EULA, stop here and check out
              <a href="https://www.minetest.net/">Minetest</a> instead!)
            </p>
          </li>
          <li>
            <p>
              Save and close the <code class="inline-code">eula.txt</code> file.
            </p>
          </li>
        </ol>
      </section>
      <section class="main-section" id="gathering_device_information">
        <header>
          <h2>
            Gathering Device Information<a
              class="header-link"
              href="#gathering_device_information"
              >#</a
            >
          </h2>
        </header>
        <p>
          You’re going to need to gather a series of unique details about your
          device and network configuration. The following steps will guide you
          through the process of finding the IPv4 address and MAC Address of
          your Server Computer, and the Default Gateway address of your router.
          Be sure to write these addresses down, as you will need each one for
          various stages of the configuration process.
        </p>
        <h3 id="server-ipv4-address">
          Finding the local IPv4 Address of the Server Computer
        </h3>
        <p>
          Each device in your home network has it’s own local IP (Internet
          Protocol) address. IP addresses are used to make sure that every
          network transmission is sent towards its intended device. We’re going
          to need to find the IP address of the device that is hosting the
          Minecraft server in order to configure our home network so that this
          device is set up to send and recieve the Minecraft server
          transmissions.
        </p>
        <h4>Windows Instructions</h4>
        <ol>
          <li>
            Open the command prompt by holding down the
            <code class="inline-code">Windows</code> key and press
            <code class="inline-code">R</code> to bring up the Run dialogue
          </li>
          <li>
            Type <code class="inline-code">cmd</code> into the Run dialogue and
            hit <code class="inline-code">Enter</code>
          </li>
          <li>
            In the command prompt, type in
            <code class="inline-code">ipconfig</code> and hit
            <code class="inline-code">Enter</code>
          </li>
          <li>
            In the console you should be able to see an overview of the Windows
            IP Configuration. Look for the line starting with
            <code class="inline-code">IPv4 Address</code> to find your IPv4
            address. Make sure to make a note of this, because you will need to
            know this later.
          </li>
        </ol>
        <h4>macOS Instructions</h4>
        <ol>
          <li>
            Open up the Terminal application from the Launchpad on macOS, or a
            terminal emulator which you prefer.
          </li>
          <li>
            In your terminal, type in
            <code class="inline-code">ipconfig getifaddr en0</code> and hit
            <code class="inline-code">Enter</code>
          </li>
          <li>
            Your device’s IP address should be printed in the output. Make sure
            to make a note of this, because you will need to know this later.
          </li>
        </ol>
        <h3 id="finding_the_physical_mac_address_of_the_server_computer">
          Finding the Physical (MAC) Address of the Server Computer
        </h3>
        <p>
          Each device has it’s own Media Access Control (MAC) address that
          operates as a unique device identifier in a network. You will need to
          know your device’s MAC Address (aka. Physical Address) to set a Static
          DHCP Lease, which will be explained in a later part of this guide.
        </p>
        <h4>Windows Instructions</h4>
        <ol>
          <li>
            Open the command prompt by holding down the
            <code class="inline-code">Windows</code> key and press
            <code class="inline-code">R</code> to bring up the Run dialogue
          </li>
          <li>
            Type <code class="inline-code">cmd</code> into the Run dialogue and
            hit <code class="inline-code">Enter</code>
          </li>
          <li>
            In the command prompt, type in
            <code class="inline-code">ipconfig</code> and hit
            <code class="inline-code">Enter</code>
          </li>
          <li>
            In the console you should be able to see an overview of the Windows
            IP Configuration. Look for the line starting with
            <code class="inline-code">Physical Address</code> to find your MAC
            address. Make sure to make a note of this, because you will need to
            know this later.
          </li>
        </ol>
        <h4>macOS Instructions</h4>
        <ol>
          <li>
            Open up the Terminal application from the Launchpad on macOS, or a
            terminal emulator which you prefer.
          </li>
          <li>
            In your terminal, type in
            <code class="inline-code">networksetup -listallhardwareports</code>
            and hit <code class="inline-code">Enter</code>
          </li>
          <li>
            <h5>If you are connected via Wi-Fi:</h5>
            <ul>
              <li>
                You will find the MAC Address below the line which says
                <code class="inline-code">Hardware Port: Wi-Fi</code>
              </li>
              <li>
                Find the address printed after the
                <code class="inline-code">Ethernet Address:</code> text. This is
                your device’s MAC Address. Make sure to make a note of this,
                because you will need to know this later.
              </li>
            </ul>
            <h5>If you are connected via Ethernet</h5>
            <ol>
              <li>
                You will find the MAC Address below the line which says
                <code class="inline-code">Hardware Port: Ethernet</code>
              </li>
              <li>
                Find the address printed after the
                <code class="inline-code">Ethernet Address:</code> text. This is
                your device’s MAC Address. Make sure to make a note of this,
                because you will need to know this later.
              </li>
            </ol>
          </li>
        </ol>
        <h3 id="finding_the_default_gateway_address">
          Finding the Default Gateway address
        </h3>
        <p>
          You’re going to need to know the address of your router on your your
          local network. This address is most commonly referred to as the
          Default Gateway address. You will need to use this address later to
          connect to your router’s management interface, which will be explained
          in the upcoming steps.
        </p>
        <h4>Windows Instructions</h4>
        <ol>
          <li>
            Open the command prompt by holding down the
            <code class="inline-code">Windows</code> key and press
            <code class="inline-code">R</code> to bring up the Run dialogue
          </li>
          <li>
            Type <code class="inline-code">cmd</code> into the Run dialogue and
            hit <code class="inline-code">Enter</code>
          </li>
          <li>
            In the command prompt, type in
            <code class="inline-code">ipconfig</code> and hit
            <code class="inline-code">Enter</code>
          </li>
          <li>
            In the console you should be able to see an overview of the Windows
            IP Configuration. Look for the line starting with
            <code class="inline-code">Default Gateway</code> to find your
            Default Gateway address. Make sure to make a note of this, because
            you will need this for the following steps.
          </li>
        </ol>
        <h4>macOS Instructions</h4>
        <ol>
          <li>
            Open up the Terminal application from the Launchpad on macOS, or a
            terminal emulator which you prefer.
          </li>
          <li>
            In your terminal, type in
            <code class="inline-code">netstat -nr | grep default</code> and hit
            <code class="inline-code">Enter</code>
          </li>
          <li>
            You should be able to see your default gateway address at the top of
            the output. Make sure to make a note of this, because you will need
            this for the following steps
          </li>
        </ol>
      </section>
      <section class="main-section" id="router_configuration">
        <header>
          <h2>
            Router Configuration<a
              class="header-link"
              href="#router_configuration"
              >#</a
            >
          </h2>
        </header>
        <p>
          Please note that the following information is presented as a general
          guide, as these following steps can vary a lot depending on which
          router you are using. If you have any trouble at any point following
          along with any of the router configurations, please make sure that you
          know the brand and model of your router so that you can easily search
          for more information online if you need.
        </p>
        <p>
          Most household routers will have a web interface that allows you to
          manage and configure services that you can connect to via the router’s
          Default Gateway Address. To configure a port forwarding rule, first we
          need to log in to the router web interface.
        </p>

        <h3>Accessing the Router Administration panel</h3>
        <p>
          With the
          <a href="#finding_the_default_gateway_address"
            >Default Gateway address that you found earlier</a
          >, we can use a web browser to navigate to the web interface that is
          used to configure your router.
        </p>
        <ol>
          <li>
            Open up a web browser, and in the navigation bar, enter the Default
            Gateway address.
          </li>
          <li>
            If the page requests for a login, the default username and password
            can be found in it’s manual, or online. You could also try using the
            username <code class="inline-code">admin</code> with the password
            being either <code class="inline-code">password</code> or
            <code class="inline-code">admin</code>, as these are usually the
            default login credentials on most household routers.
          </li>
        </ol>
        <h3 id="setting_a_static_dhcp_lease_for_the_server_computer">
          Setting a Static DHCP lease for the Server Computer
        </h3>
        <p>
          DHCP (Dynamic Host Configuration Protocol) is a very commonly used
          routing service that automatically provides each device on a network
          with their own IP address. If your router uses DHCP (Which it most
          likely does), you’re going to want to set a static DHCP lease to make
          sure that the Server Computer’s IP address doesn’t change, or else you
          will have to reconfigure your Router and Minecraft Server with a new
          address each time it changes.
        </p>
        <ol>
          <li>
            In the router configuration panel, you will need to find the DHCP
            menu. If you can’t find it, you might be able to find it under
            Advanced Mode, or under a Services or Routing category. If you still
            can’t find it, try searching online for static DHCP lease
            configuration guides for your specific router model.
          </li>
          <li>
            Under the list of Static Leases, Add a Static Lease for your Server
            Computer with the following settings:
            <ul>
              <li>
                Set the Host Name to a short name for your device (e.g.
                <code class="inline-code">home-pc</code>,
                <code class="inline-code">server-pc</code>, or
                <code class="inline-code">kyles-laptop</code>)
              </li>
              <li>
                Set the MAC Address to the
                <a href="#server-mac-address"
                  >Physical Address of the Server Computer</a
                >
                which you found earlier.
              </li>
              <li>
                Set the IP address to the
                <a href="#server-ipv4-address"
                  >IPv4 address of the Server Computer</a
                >
                which you found earlier.
              </li>
            </ul>
          </li>
        </ol>
        <h3>Port Forwarding</h3>
        <p>
          In order to allow people from outside your local network to connect to
          your Minecraft Server, you will need to configure a port forwarding
          rule in your router’s administration dashboard.
        </p>
        <p>
          In the following steps, we will be configuring a Port Forwarding rule
          which will allow people from outside your private local-area network
          to connect to a specific service on your computer (in this case, a
          Minecraft server) over the internet, without exposing the rest of your
          private network to the outside world.
        </p>
        <ol>
          <li>
            In the router configuration panel, you will need to find the Port
            Forwarding menu. If you can’t find it, you might be able to find it
            under Advanced Mode, a Games and Services category, or under
            Firewall settings. If you still can’t find it, try searching online
            for port forwarding guides for your specific router model.
          </li>
          <li>
            Once you have found the Port Forwarding options, You’re going to
            need to create a new Port Forwarding rule with the following
            settings:
            <ul>
              <li>
                Set the configuration name to
                <code class="inline-code">Minecraft</code>
              </li>
              <li>
                Set the WAN and LAN port to
                <code class="inline-code">25565</code>
              </li>
              <li>
                Make sure the protocol is set to both
                <code class="inline-code">UDP</code> and
                <code class="inline-code">TCP</code>
              </li>
              <li>
                Set the destination IP address to the
                <a href="#server-ipv4-address"
                  >IPv4 address of the Server Computer</a
                >
                which you found earlier.
              </li>
            </ul>
          </li>
        </ol>
        <p>
          Remember to save the changes that you have made to your router before
          you proceed.
        </p>
      </section>
      <section class="main-section" id="configuring_the_server_properties">
        <header>
          <h2>
            Configuring the Server Properties<a
              class="header-link"
              href="#configuring_the_server_properties"
              >#</a
            >
          </h2>
        </header>
        <p>
          In this step, we will be configuring the rest of the server files so
          that the Minecraft server can be fully operational. For this, you will
          need to return to the server computer that you downloaded the
          Minecraft server files onto. Please make sure that you can access the
          server directory that you made earlier in the
          <a href="#getting_started">Getting Started</a> section before you
          proceed.
        </p>
        <p>
          In the server directory, you should be able to see a file called
          <code class="inline-code">server.properties</code>. Open this file
          with a text editor (e.g. You can use Notepad on Windows or TextEdit on
          macOS, or vim) and edit it to suit your needs.
        </p>
        <ul>
          <li>
            Edit the line starting with
            <code class="inline-code">server-ip=</code> and make sure it equals
            your server computer’s IPv4 address
          </li>
          <li>
            Players who are given the Operator permission by default have the
            ability to stop the server from within the game. If you don’t want
            this to happen, you can change the line with
            <code class="inline-code">op-permission-level=4</code> to
            <code class="inline-code">op-permission-level=3</code>
          </li>
          <li>
            If you don’t want random people to end up on your server, make sure
            to change <code class="inline-code">white-list=false</code> to
            <code class="inline-code">white-list=true</code>. This allows you to
            configure a list of approved players by adding and removing
            usernames. Users who are not in the list will not be able to connect
            to the server. You will learn how to configure this list later on in
            this guide.
          </li>
          <li>
            If you don’t want players whacking each other, change
            <code class="inline-code">pvp=true</code> to
            <code class="inline-code">pvp=false</code>
          </li>
          <li>
            If you would like to play Creative mode instead of Survival, change
            <code class="inline-code">gamemode=survival</code> to
            <code class="inline-code">gamemode=creative</code>
          </li>
          <li>
            Alternatively, if you wish to play Hardcore, keep
            <code class="inline-code">gamemode=survival</code> and set
            <code class="inline-code">hardcore=true</code>
          </li>
          <li>
            If you don’t like hostile monsters, or you’d like to crank up the
            difficulty, change
            <code class="inline-code">difficulty=easy</code> to either
            <code class="inline-code">peaceful</code>,
            <code class="inline-code">easy</code>,
            <code class="inline-code">normal</code>, or
            <code class="inline-code">hard</code>
          </li>
          <li>
            If you trust the players on your server not to use exploits,
            glitches, and hacked clients to give them unfair mobility
            advantages, I recommend changing
            <code class="inline-code">allow-flight=false</code> to
            <code class="inline-code">allow-flight=true</code> because I have
            had many problems with false positives where the server will
            automatically kick people for flying when they were not.
          </li>
          <li>
            You can set a custom MOTD (Message Of The Day) which will display a
            short description about your server. To change this, edit the line
            starting with
            <code class="inline-code">motd=A Minecraft Server</code>. If you
            would like to make it look very fancy, I recommend using the
            <a href="https://minecraft.tools/en/motd.php">MOTD Generator</a>
            from <a href="https://minecraft.tools">minecraft.tools</a>, which
            will allow you to easily customise the MOTD so that you can generate
            the text to copy and paste into the
            <code class="inline-code">motd=</code> section of the properties
            file.
          </li>
        </ul>
        <p>
          To find out more information about the server.properties file, please
          refer to the
          <a
            href="https://minecraft.fandom.com/wiki/Server.properties#Minecraft_server_properties"
            >Minecraft Wiki Page</a
          >.
        </p>
      </section>
      <section class="main-section" id="launching_the_server">
        <header>
          <h2>
            Launching the Server<a
              class="header-link"
              href="#launching_the_server"
              >#</a
            >
          </h2>
        </header>
        <p>
          Now that we have configured the server, we can now start it and begin
          to play on a new world.
        </p>
        <p>
          There are two recommended ways to start the server. The first and
          easiest method is to execute the
          <code class="inline-code">server.jar</code> file by:
        </p>
        <ol>
          <li>
            Right clicking on the
            <code class="inline-code">server.jar</code> file
          </li>
          <li>Select <code class="inline-code">Open With…</code></li>
          <li>
            Open the <code class="inline-code">server.jar</code> file with the
            Java Runtime Environment
          </li>
        </ol>
        <p>
          This will launch the server, and in most cases, this should be all you
          need. However, if you have a more powerful server computer and you
          would like more people to be able to play on your world at a time, you
          may wish to allocate more RAM to the server. In order to do that, you
          will need to create a startup script to run your server through a
          terminal.
        </p>
        <h3>Launching the server with a script</h3>
        <p>
          In the following steps, we will be specifying an amount of RAM for the
          Minecraft server, and we will launch the server using a script.
        </p>
        <h4>Windows Instructions</h4>
        <ol>
          <li>
            Right click in your server folder and create a new text document.
          </li>
          <li>
            In the view panel of the file explorer, make sure that
            <code class="inline-code">Show File Extensions</code> is ticked
          </li>
          <li>
            Rename the <code class="inline-code">New Text Document.txt</code> to
            <code class="inline-code">server.bat</code>
          </li>
          <li>
            Right click on the <code class="inline-code">server.bat</code> file,
            and select <code class="inline-code">Edit</code>
          </li>
          <li>
            In the notepad window, enter the following lines:
            <code class="multiline-code"
              >@echo off java -Xms2G -Xmx4G -jar server.jar nogui PAUSE
            </code>
          </li>
          <li>
            Change the lines to suit your needs as follows:
            <ul>
              <li>
                The <code class="inline-code">-Xms</code> option is the amount
                of RAM that the server will start with. I recommend 2 GB for an
                unmodified (vanilla) server.
              </li>
              <li>
                The <code class="inline-code">-Xmx</code> option is the maximum
                amount of RAM that the server can use. I recommend 4 GB for an
                unmodified (vanilla) server.
              </li>
              <li>
                Change the <code class="inline-code">server.jar</code> so that
                it matches the full name of the
                <code class="inline-code"
                  >minecraft_server.&lt;version&gt;.jar</code
                >
                file in your server directory.
              </li>
              <li>
                If you wish to use the original GUI to monitor your server,
                remove the <code class="inline-code">nogui</code> argument. Note
                that if you choose to remove this option, there will be both a
                terminal window and a graphical server interface running
                concurrently, and closing either of them will stop the server.
              </li>
              <li>
                The <code class="inline-code">PAUSE</code> command is to prevent
                the terminal window from closing immediately after the server
                process has stopped, either manually or as the result of a
                server crash. It will wait for you to press a key before the
                terminal closes. If you do not think you need this, you may
                remove that line, however I recommend that you keep it there, as
                it can be very useful to read over the lines that followed up to
                a crash if one were to occur.
              </li>
            </ul>
          </li>
          <li>
            Save the <code class="inline-code">server.bat</code> file, and
            double click on it to run the minecraft server.
          </li>
        </ol>
      </section>
      <!-- <section class="main-section" id="connecting_to_the_server">
            <header>
                <h2>Connecting to the Server</h2>
            </header>
            <p>text</p>
            <p>text</p>
        </section> -->
    </main>
    <footer>
      This <a href="https://freecodecamp.org">FreeCodeCamp</a> project was
      developed by <a href="https://pkyle.me">Kyle Phillips.</a><br />
      This guide is licensed under a
      <a href="https://creativecommons.org/licenses/by-sa/4.0/"
        >Creative Commons Attribution-ShareAlike 4.0 International License</a
      ><br />
      This content is unofficial and not endorsed by Mojang.<br />
      Copyright 2022 Kyle Phillips
    </footer>
  </body>
</html>
